# BazyDanychIIUG

Tutaj znajdują się przykłady aplikacji typu REST API, które łączą się z nierelacyjnymi bazami danych.

## Uwaga

Podane przykłady nie korzystają z asynchroniczności. Użycie asynchroniczności jest wymagane (można do tego użyć innych pakietów) i wynika ze znajomości poprzednich przedmiotów.

### Przykłady aplikacji:

- W katalogu `mern` znajduje się przykład aplikacji typu REST API, korzystającą z `express.js` oraz `mongodb`.
- W katalogu `FlaskNeo4J_Example` znajduje się przykład aplikacji typu REST API, korzystającą z `flask` oraz `neo4j`.
